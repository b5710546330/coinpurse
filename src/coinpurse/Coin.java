package coinpurse;
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Pongsathorn Phakdeethai
 * @version 24.02.2015
 */

public class Coin extends AbstractValuable {
    
	private double value;
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
	
    public Coin( double value ) {
        this.value = value;
    }
    /**
     * @return value of coin.
     */
    public double getValue ()
	{
		return this.value;
	}

    /**
     * @return String.
     */
    public String toString() {
    	return this.getValue()+"-Baht";
    }
    
}
