package coinpurse;
import java.util.HashMap;
import java.util.Map;
/**
 * A money with a monetary value.
 * You can't change the value of a money.
 * @author Pongsathorn Phakdeethai
 * @version 24.02.2015
 */

public class Coupon extends AbstractValuable {

	/** Color of the coupon */
	private String color;
	/** Map of the coupon */
	Map <String, Double > map = new HashMap <String,Double>();
	/** 
	 * Constructor for a new coupon. 
	 * @param color is the Color for the coupon
	 */
	public Coupon(String color)
	{
		this.color = color.toLowerCase();
		map.put("red", 100.0);
		map.put("blue", 50.0);
		map.put("green", 20.0);
	}
	/**
	 * @return color of the coupon
	 */
	public double getValue ()
	{
		return map.get(color);
	}

	/**
	 * @return String
	 */
	public String toString()
	{
		return String.format(color+" Coupon");
	}
}
