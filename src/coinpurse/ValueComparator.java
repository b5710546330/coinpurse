package coinpurse;

import java.util.Comparator;

/**
 * Comparing between both values and find difference both of them.
 * @author Leomaru
 *
 */

public class ValueComparator implements Comparator<Valuable> {
	public int compare(Valuable a, Valuable b) {
		// compare them by value. This is easy. 
		return (int)(b.getValue()-a.getValue());
	}
}
