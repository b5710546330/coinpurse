package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import coinpurse.Valuable;
/**
 * Withdraw with Greedy algorithm.
 * @author Pongsathorn Phakdeethai
 */

public class GreedyWithdraw implements WithdrawStrategy {
	

	/**
	 * Withdraw amount of money requested.
	 * Return an array of money withdraw form purse,
	 * or return null if it cannot withdraw amount of money requested.
	 */
	public Valuable[] withdraw( double amount , List<Valuable> valuables ) {
		//TODO don't allow to withdraw amount < 0
		//if ( ??? ) return ???;
		ArrayList<Valuable> SortAlready = new ArrayList<Valuable>();
		SortAlready.addAll(valuables);

		if(amount < 0 )
		{
			return null;
		}
		/*
		 * One solution is to start from the most valuable coin
		 * in the purse and take any coin that maybe used for
		 * withdraw.
		 * Since you don't know if withdraw is going to succeed, 
		 * don't actually withdraw the coins from the purse yet.
		 * Instead, create a temporary list.
		 * Each time you see a coin that you want to withdraw,
		 * add it to the temporary list and deduct the value
		 * from amount. (This is called a "Greedy Algorithm".)
		 * Or, if you don't like changing the amount parameter,
		 * use a local total to keep track of amount withdrawn so far.
		 * 
		 * If amount is reduced to zero (or tempTotal == amount), 
		 * then you are done.
		 * Now you can withdraw the coins from the purse.
		 * NOTE: Don't use list.removeAll(templist) for this
		 * becuase removeAll removes *all* coins from list that
		 * are equal (using Coin.equals) to something in templist.
		 * Instead, use a loop over templist
		 * and remove coins one-by-one.		
		 */



		// Did we get the full amount?
		if ( amount > 0 )
		{	// failed. Since you haven't actually remove
			// any coins from Purse yet, there is nothing
			// to put back.
			ArrayList<Valuable> withdraw = new ArrayList();
			for (int i = 0 ; i < SortAlready.size() ;i++)
			{
				if (amount >= SortAlready.get(i).getValue())
				{
					withdraw.add(SortAlready.get(i));
					amount -= SortAlready.get(i).getValue();
				}
			}

			if(amount == 0)
			{
				System.out.println(withdraw.size()+"size");
				Valuable[] coins = new Valuable[withdraw.size()];
				for (int i = 0 ; i < coins.length ; i++)
				{
					coins[i] =  withdraw.get(i);
				}
				for (int i = 0 ; i < coins.length ;i++)
				{
					valuables.remove((Valuable)withdraw.get(i));
				}
				return coins;
			}
			return null;
		}
		return null;

		// Success.
		// Since this method returns an array of Coin,
		// create an array of the correct size and copy
		// the Coins to withdraw into the array.
		//TODO replace this with real code
	}

}
