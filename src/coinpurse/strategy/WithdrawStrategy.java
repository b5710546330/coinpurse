package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;

/**
 * A Stratgy interface that collect withdraw method to use in purse class.
 * @author Pongsathorn Phakdeethai.
 * @version 24.02.2015
 */
public interface WithdrawStrategy {
	/**
	 * 
	 * @param amount of money you want to withdraw.
	 * @param valuables is a lots of valuable in purse class that you collect them.
	 * @return Valuable[] is valuables after withdraw already.
	 */

	public Valuable[] withdraw(double amount , List<Valuable> valuables);
	
}
