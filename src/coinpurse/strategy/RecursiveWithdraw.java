package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;

public class RecursiveWithdraw implements WithdrawStrategy{

	/**
	 * List of valuables.
	 */
	List<Valuable> valuables;

	/**
	 * If you success withdrawal you will remove it from list for update your balance.
	 */
	public Valuable[] withdraw (double amount , List<Valuable> valuables)
	{
		this.valuables = valuables;
		List<Valuable> remove = RecursiveWithdraw (amount , 0);
		if(remove != null)
		{
			System.out.println(remove.size()+"size");
			Valuable[] coins = new Valuable[remove.size()];
			for (int i = 0 ; i < coins.length ; i++)
			{
				coins[i] =  remove.get(i);
			}
			for (int i = 0 ; i < coins.length ;i++)
			{
				valuables.remove((Valuable)remove.get(i));
			}
			return coins;
		}
		return null;
	}

	/**
	 * RucursiveWithdraw : Check the way that you surely can't withdraw.
	 * And you will go another way because you have tow choices that pick or doesn't pick.
	 * If you go another way you can and can withdraw it you will collect it in 'temp'.
	 * And do it again and again until amount that you want to withdraw = 0 that success withdraw.
	 * @param amount is amount of money that you want to withdraw.
	 * @param index is use to check that you out of bound or not yet.
	 * @return temperature of list of valuable that use collect amount when to success withdraw.
	 */
	public List<Valuable> RecursiveWithdraw (double amount , int index)
	{
		if (index > valuables.size()) return null;
		else if (amount < 0) return null;
		else if (amount == 0) return new ArrayList<Valuable>();

		List<Valuable> temp =  RecursiveWithdraw (amount - valuables.get(index).getValue() , index+1);
		if (temp != null)
		{
			temp.add( valuables.get(index));
		}

		else if (temp == null)
		{
			temp = RecursiveWithdraw (amount,index+1);
		}

		return temp;

	}

}

