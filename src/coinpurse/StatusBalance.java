package coinpurse;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

public class StatusBalance extends JFrame implements Observer {

	JLabel balanceLabel ;
	JProgressBar progressbar;
	public StatusBalance ()
	{
		super("Purse Status");
		initialize();
	}

	/**
	 * Initial windows of Status of balance.
	 */
	public void initialize()
	{
		balanceLabel = new JLabel();
		progressbar = new JProgressBar();	
		super.getContentPane().setLayout(new GridLayout());
		super.getContentPane().add(balanceLabel);
		super.getContentPane().add(progressbar);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * Update status of balance. (Full , Empty or How many you have items.)
	 */
	public void update(Observable sub , Object Info )
	{
		Purse purse = (Purse)sub ;

		if (purse.isFull())
		{
			balanceLabel.setText("FULL");
		}
		else if (!purse.isFull())
		{
			if (purse.count()!=0)
			{
				balanceLabel.setText(purse.count()+"");
			}
			else if (purse.count()==0)
			{
				balanceLabel.setText("EMPTY");
			}	
		}
		progressbar.setMaximum(purse.getCapacity()); //maximum of progressbar
		progressbar.setMinimum(0); // minimum of progressbar
		progressbar.setValue(purse.count()); // current item in purse
		
	}
	
	/**
	 * Make you can see 'Purse Status'.
	 */
	public void run ()
	{
		setVisible(true);
	}

}
