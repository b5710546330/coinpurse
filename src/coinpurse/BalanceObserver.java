package coinpurse;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class BalanceObserver extends JFrame implements Observer{
	public BalanceObserver()
	{
		super("Purse Balance");
	}
	
	JLabel balanceLabel;
	
	/**
	 * Construture class.
	 */
	public void initialize()
	{
		balanceLabel = new JLabel(0+" Baht");
		super.getContentPane().setLayout(new GridLayout());
		super.getContentPane().add(balanceLabel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * Update balance when depositing or withdrawing.
	 */
	public void update (Observable subject , Object info)
	{
		if (subject instanceof Purse)
		{
			Purse purse = (Purse) subject;
			int balance = (int) purse.getBalance();
			balanceLabel.setText(balance+ " Baht");
			//super.repaint();
			
		}
		if (info != null)
		System.out.println(info);
	}
	
	/**
	 * Make you can see 'Purse Balance'.
	 */
	public void run ()
	{
		initialize();
		setVisible(true);
	}
}