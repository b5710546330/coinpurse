package coinpurse;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Pongsathorn Phakdeethai
 *
 */
public class ListUtil {

	public static void printList(List<?> list)
	{
		if (list.size()!=0)
		{
			if (list.size()==1)
			{
				System.out.print(list.get(0));
			}
			else 
			{
				System.out.print(list.get(0)+", ");
			}
			printList(list.subList(1, list.size()));
		}
	}
	/**
	 * Find the largest element in a List of String,
	 * using the String compareTo method.
	 * @return the lexically largest element in the List 
	 */

	private static String max (List<String> list)
	{
		if (list == null)
		{
			return null;
		}
		else if (list.size()==1)
		{
			return list.get(0);	
		}
		else if (list.get(list.size()-1).compareTo(list.get(0))>0)
		{
			return max(list.subList(1, list.size()));
		}
		else 
			return max(list.subList(0, list.size()-1));
	}


public static void main (String[] args)
{
	List<String> list;
	//if any command line args, then use them as the list!
	if (args.length > 0) list = Arrays.asList(args);
	else list = Arrays.asList("bird" , "zebra" , "cat" , "dragon");

	System.out.print("List comtains: ");
	printList(list);

	String max = max(list);
	System.out.println("\nLexically greatest element is "+max);
}
}
