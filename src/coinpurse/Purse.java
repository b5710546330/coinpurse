package coinpurse; 
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.WithdrawStrategy;
//TODO import ArrayList and Collections (for Collections.sort())

/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Pongsathorn Phakdeethai
 */
public class Purse extends Observable {
	/** Collection of coins in the purse. */
	private List<Valuable> money = new ArrayList<Valuable>();
	private WithdrawStrategy withdrawStrategy ;
	/** Capacity is maximum NUMBER of value the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of money you can put in purse.
	 */
	public Purse( int capacity ) {
		//TODO initialize the attributes of purse
		this.capacity = capacity;
	}

	/**
	 * Count and return the number of money in the purse.
	 * This is the number of money, not their value.
	 * @return the number of money in the purse
	 */
	public int count() { return money.size(); }

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() 
	{
		double balance = 0;
		for (int i = 0 ; i < money.size() ; i++ )
		{
			balance += money.get(i).getValue();
		}
		return balance;
	}


	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */
	//TODO write accessor method for capacity. Use Java naming convention.
	public int getCapacity() 
	{ 
		return this.capacity; 
	}

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		//TODO complete this method
		//TODO Don't Repeat Yourself: Avoid writing duplicate code.
		if (money.size() == capacity)
		{
			return true;
		}
		else return false;
	}

	/** 
	 * Insert a banknote or coupon or coin into the purse.
	 * The banknote or coupon or coin is only inserted if the purse has space for it
	 * and the banknote or coupon or coin has positive value.  No worthless coins!
	 * @param value is a Valuable object to insert into purse
	 * @return true if banknote or coupon or coin inserted, false if can't insert
	 */
	public boolean insert( Valuable value ) {
		// if the purse is already full then can't insert anything.

		//TODO complete the insert method
		if (this.isFull())
		{
			return false;
		}
		else 
		{
			if (value.getValue() > 0)
			{
				money.add(value);
				super.setChanged();
				super.notifyObservers(this); //parameter is optional
				return true;
			}
			else return false;	
		}
	}

	/**
	 * 
	 * @param amount of money you want to withdraw.
	 * @param valuables is a lots of valuable in purse class that you collect them.
	 * @return Valuable[] is valuables after withdraw already.
	 */
	public void setWithdrawStrategy (WithdrawStrategy withdrawstrategy)
	{
		this.withdrawStrategy = withdrawstrategy;
		
	}
	public Valuable[] withdraw( double amount ) 
	{
		return withdrawStrategy.withdraw(amount, money);
	}

	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 */
	public String toString() {
		//TODO complete this
		return money.size()+"coins with value"+getBalance();
	}

}
//TODO remove the TODO comments after you complete them.
//TODO When you are finished, there should not be any TODO. Including this one.
