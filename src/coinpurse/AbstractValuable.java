package coinpurse;

public abstract class AbstractValuable implements Valuable {
	
	/**
     * compare by value
     * @param values is a values to use to compare.
     * @return boolean that what is boolean from comparing.
     */
	
	public int compareTo (Valuable values)
	{
		return this.toString().compareTo(values.toString());
	}
	
	 /**
     * check conditions by object arg 
     * @param arg is an Object to compare to null, class and catch to be a Coin to compare value
     * @return false if arg equals to null or arg's class doesn't equals to coin's class or arg's value equals to coin's value
     */
	
	public boolean equals (Object arg)
	{
		AbstractValuable other = (AbstractValuable)arg;
    	if (arg == null || arg.getClass() != this.getClass() || other.getValue() != this.getValue() )
    		return false;
    	else return true;	
	}

}
