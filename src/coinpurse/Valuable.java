package coinpurse;
/**
 * this interface use for return value.
 * @author Pongsathorn Phakdeethai
 */
public interface Valuable extends Comparable<Valuable> {
	/*
	 * return value
	 */
	public double getValue( );
}
