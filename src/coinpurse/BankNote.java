package coinpurse;
/**
 * A money with a monetary value.
 * You can't change the value of a Banknote.
 * @author Pongsathorn Phakdeethai
 * @version 24.02.2015
 */
public class BankNote extends AbstractValuable {
	/** Value of the banknote */
	private double value;
	
	/** serialnumber of the banknote */
	private int serialnumber;
	/** Start new serialnumber at 1million */
	private static int nextserialnumber = 1000000;

	/** 
     * Constructor for a new Banknote.
     * getnextserialnumber (serialnumber+1). 
     * @param value is the Color for the value
     */
	public BankNote (double value)
	{
		this.value = value;
		serialnumber = nextserialnumber;
		nextserialnumber ++;
	}
	
	/**
	 * @return value of BankNote.
	 */
	
	public double getValue()
	{
		return this.value;
	}
	
	/**
     * @return nextserialnumber
     */
	public int getNextserialnumber()
	{
		return this.nextserialnumber;
	}
	
	/**
	 * @return String
	 */
	public String toString() {
    	return String.format("%.2f Baht Banknote [%d]", this.getValue(),this.serialnumber );
    }
}
