package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class PurseObsever implements Observer {
	
	public PurseObsever ()
	{
		// no reference to purse necessary.
	}
	
	/**
	 * update receives notification from the purse.
	 */
	public void update (Observable subject , Object info)
	{
		if (subject instanceof Purse)
		{
			Purse purse = (Purse) subject;
			int balance = (int) purse.getBalance();
			System.out.println("Balance is: "+ balance);
		}
		if (info != null)
		System.out.println(info);
	}
}
