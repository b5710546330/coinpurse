package coinpurse; 

import coinpurse.strategy.RecursiveWithdraw;

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Pongsathorn Phakdeethai
 * @version 24.02.2015
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
//TODO follow the steps in the sequence diagram
        // 1. create a Purse
    	Purse purse  = new Purse (3);
    	RecursiveWithdraw  recursivewithdraw = new RecursiveWithdraw ();
    	BalanceObserver balanceObserver = new BalanceObserver ();
    	StatusBalance status = new StatusBalance ();
    	purse.addObserver(balanceObserver);
    	purse.addObserver(status);
    	purse.setWithdrawStrategy(recursivewithdraw);
    	status.run();
    	balanceObserver.run();
    	ConsoleDialog console = new ConsoleDialog (purse);
    	console.run();
    	
        // 2. create a ConsoleDialog with a reference to the Purse object

        // 3. run() the ConsoleDialog

    }
}
